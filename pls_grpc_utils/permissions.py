import jwt
from django.conf import settings
from django.views import View
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import BasePermission
from rest_framework.request import Request


class LandingOrPLSAuthenticated(BasePermission):

    def has_permission(self, request, view):
        print('request.pls_token', request.pls_token)
        return (hasattr(request, 'pls_token') and bool(request.pls_token)) or (hasattr(request, 'is_landing') and request.is_landing)


class IsPLSAuthenticated(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return hasattr(request, 'pls_token') and bool(request.pls_token)


def validate_module_permission(token_data):
    if token_data.get('is_admin'):
        return True
    for module in token_data.get('modules', []):
        if module.split('/')[0] == settings.PLS_MODULE_NAME:
            return True
    return False


class IsHasModulePermission(BasePermission):

    def has_permission(self, request: Request, view: View) -> bool:
        token = getattr(request, 'pls_token', None)
        if token:
            return validate_module_permission(token)
        return False


class AuthorizeModuleMixin(object):

    def create(self, *args, **kwargs):
        data = super(AuthorizeModuleMixin, self).create(*args, **kwargs)
        token = jwt.decode(data['access_token'], verify=False)
        if not validate_module_permission(token):
            raise ValidationError({'username': 'У вас нет доступа к этому ресурсу'})
        return data


class OwnerPermission(BasePermission):

    def has_permission(self, request: Request, view: View) -> bool:
        token = getattr(request, 'pls_token', None)
        if token:
            return token.get('account_owner_id', None) == token.get('user_id', 0)
        return False


class HasAccountPermission(BasePermission):

    def has_permission(self, request: Request, view: View) -> bool:
        REQUIRED_ACCOUNT_PERMISSIONS = getattr(view, 'REQUIRED_ACCOUNT_PERMISSIONS', False)
        assert REQUIRED_ACCOUNT_PERMISSIONS, 'View has no REQUIRED_ACCOUNT_PERMISSIONS specified'
        token = getattr(request, 'pls_token', None)
        if token:
            current_permission = token.get('permissions', [])
            return token.get('account_owner_id', None) == token.get('user_id', 0) or all(permission in current_permission for permission in REQUIRED_ACCOUNT_PERMISSIONS)
        return False

