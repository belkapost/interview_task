import json

import grpc
from rest_framework.exceptions import APIException

from sentry_sdk import capture_exception


class CustomAPiException(APIException):

    def __init__(self, detail=None, code=None, status_code=None):
        super(CustomAPiException, self).__init__(detail=detail, code=code)
        if status_code:
            self.status_code = status_code


def grpc_errors_wrapper(func):
    def wrapped_function(slf, *args, **kwargs):
        try:
            return func(slf, *args, **kwargs)
        except grpc.RpcError as error:
            _code = error.code()
            _detail = error.details()
            print('_detail', _detail)
            try:
                _detail = json.loads(_detail)
            except:
                pass
            if _code == grpc.StatusCode.UNKNOWN:
                capture_exception()
                raise CustomAPiException(status_code=500, detail=_detail)
            elif _code == grpc.StatusCode.INVALID_ARGUMENT:
                raise CustomAPiException(status_code=400, detail=_detail)
            elif _code == grpc.StatusCode.DEADLINE_EXCEEDED:
                capture_exception()
                raise CustomAPiException(status_code=504, detail=_detail)
            elif _code == grpc.StatusCode.NOT_FOUND:
                raise CustomAPiException(status_code=404, detail=_detail)
            elif _code == grpc.StatusCode.ALREADY_EXISTS:
                raise CustomAPiException(status_code=409, detail=_detail)
            elif _code == grpc.StatusCode.PERMISSION_DENIED:
                raise CustomAPiException(status_code=403, detail=_detail)
            elif _code == grpc.StatusCode.UNAUTHENTICATED:
                raise CustomAPiException(status_code=401, detail=_detail)
            elif _code == grpc.StatusCode.RESOURCE_EXHAUSTED:
                raise CustomAPiException(status_code=429, detail=_detail)
            elif _code == grpc.StatusCode.FAILED_PRECONDITION:
                raise CustomAPiException(status_code=400, detail=_detail)
            elif _code == grpc.StatusCode.ABORTED:
                raise CustomAPiException(status_code=400, detail=_detail)
            elif _code == grpc.StatusCode.OUT_OF_RANGE:
                raise CustomAPiException(status_code=413, detail=_detail)
            elif _code == grpc.StatusCode.UNIMPLEMENTED:
                capture_exception()
                raise CustomAPiException(status_code=501, detail=_detail)
            elif _code == grpc.StatusCode.INTERNAL:
                capture_exception()
                raise CustomAPiException(status_code=500, detail=_detail)
            elif _code == grpc.StatusCode.UNAVAILABLE:
                capture_exception()
                print(error)
                raise CustomAPiException(status_code=503, detail=_detail)
            elif _code == grpc.StatusCode.DATA_LOSS:
                capture_exception()
                raise CustomAPiException(status_code=500, detail=_detail)
            elif _code == grpc.StatusCode.CANCELLED:
                raise CustomAPiException(status_code=500, detail=_detail)

    return wrapped_function
