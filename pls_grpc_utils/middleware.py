import jwt
from rest_framework import HTTP_HEADER_ENCODING
from rest_framework import exceptions

from django.conf import settings

AUTH_HEADER_TYPES = ('Bearer',)
AUTH_HEADER_TYPE_BYTES = set(
    h.encode(HTTP_HEADER_ENCODING)
    for h in AUTH_HEADER_TYPES
)


class PLSJWTMiddleware:
    header = "HTTP_AUTHORIZATION"

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        self.process_request(request)
        response = self.get_response(request)
        return response

    def process_request(self, request):
        header = self.get_header(request)
        if not self.is_landing(request):
            if header is None:
                return None

            raw_token = self.get_raw_token(header)
            if raw_token is None:
                return None
        else:
            raw_token = settings.LANDING_ACCESS_TOKEN
        print(raw_token)
        token_data = self.get_token_data(raw_token)
        request.pls_token = token_data
        request.access_token = raw_token
        return

    def get_header(self, request):
        """
        Extracts the header containing the JSON web token from the given
        request.
        """
        header = request.META.get('HTTP_AUTHORIZATION')

        if isinstance(header, str):
            header = header.encode(HTTP_HEADER_ENCODING)

        return header

    def get_raw_token(self, header):
        """
        Extracts an unvalidated JSON web token from the given "Authorization"
        header value.
        """
        parts = header.split()

        if len(parts) == 0:
            return None

        if parts[0] not in AUTH_HEADER_TYPE_BYTES:
            return None

        if len(parts) != 2:
            raise exceptions.AuthenticationFailed(
                'Authorization header must contain two space-delimited values',
                code='bad_authorization_header',
            )

        return parts[1]

    def get_token_data(self, token):
        try:
            return jwt.decode(token, settings.PLS_JWT_SECRET_KEY, algorithms=['HS256'])
        except jwt.InvalidTokenError:
            return None

    def is_landing(self, request):
        header = request.META.get('HTTP_API_KEY')
        return header == settings.LANDING_API_KEY
