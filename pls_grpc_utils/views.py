from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.settings import api_settings


class GRPCBaseView(APIView):
    serializer_class = None
    list_query_params = []

    def querydict_to_dict(self, query_dict):
        data = {}
        for key in query_dict.keys():
            v = query_dict.getlist(key)
            if key not in self.list_query_params and len(v) == 1:
                v = v[0]
            data[key] = v
        return data

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.

        You may want to override this if you need to provide different
        serializations depending on the incoming request.

        (Eg. admins get full serialization, others get basic serialization)
        """
        assert self.serializer_class is not None, (
                "'%s' should either include a `serializer_class` attribute, "
                "or override the `get_serializer_class()` method."
                % self.__class__.__name__
        )

        return self.serializer_class

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }

    def perform_create(self, serializer):
        serializer.create()


class GRPCCreateView(GRPCBaseView):

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class GRPCListView(GRPCBaseView):

    def get_data(self):
        return self.querydict_to_dict(self.request.query_params)

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.get_data(), many=True)
        serializer.is_valid(raise_exception=True)
        serializer.list()
        return Response(serializer.data)


    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class GRPCRetrieveView(GRPCBaseView):

    lookup_field = None
    lookup_url_kwarg = None

    def get_object(self):
        assert self.lookup_field is not None, (
                "'%s' should include a `lookup_field` attribute."
                % self.__class__.__name__
        )

        assert self.lookup_url_kwarg is not None, (
                "'%s' should include a `lookup_url_kwarg` attribute."
                % self.__class__.__name__
        )
        return {self.lookup_field: self.request.parser_context['kwargs'].get(self.lookup_url_kwarg)}


    def retrieve(self, request, *args, **kwargs):
        data = self.get_object()
        if (request.query_params):
            data.update(self.querydict_to_dict(request.query_params))
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.retrieve()
        return Response(serializer.data)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class GRPCDestroyView(GRPCBaseView):
    lookup_field = None
    lookup_url_kwarg = None

    def get_object(self):
        return {self.lookup_field: self.request.parser_context['kwargs'].get(self.lookup_url_kwarg)}

    def delete(self, request, *args, **kwargs):
        data = {**request.data, **self.get_object()}
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.delete()
        return Response(serializer.data)


class GRPCUpdateView(GRPCBaseView):

    lookup_field = None
    lookup_url_kwarg = None

    def get_object(self):
        assert self.lookup_field is not None, (
                "'%s' should include a `lookup_field` attribute."
                % self.__class__.__name__
        )

        assert self.lookup_url_kwarg is not None, (
                "'%s' should include a `lookup_url_kwarg` attribute."
                % self.__class__.__name__
        )
        return {self.lookup_field: self.request.parser_context['kwargs'].get(self.lookup_url_kwarg)}

    def update(self, request, *args, **kwargs):
        data = {**request.data, **self.get_object()}
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.update()
        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
