import grpc
from google.protobuf.json_format import MessageToDict, ParseDict
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.utils.serializer_helpers import ReturnDict

from .error_wrapper import grpc_errors_wrapper

LIST_SERIALIZER_KWARGS = (
    'read_only', 'write_only', 'required', 'default', 'initial', 'source',
    'label', 'help_text', 'style', 'error_messages', 'allow_empty',
    'instance', 'data', 'partial', 'context', 'allow_null'
)


def get_metadata(access_token):
    return (('accesstoken', access_token), )


def metadata_from_request(request):
    metadata = ()
    access_token = getattr(request, 'access_token', None)
    language_code = getattr(request, 'LANGUAGE_CODE', 'ru')
    if access_token:
        metadata += ('accesstoken', access_token),
    if language_code:
        metadata += ('languagecode', language_code),
    return metadata


class GrpcSerializer(serializers.Serializer):
    INCLUDE_TOKEN_DATA = True

    class Meta:
        host = None
        stub = None
        method = None
        request = None

    @classmethod
    def many_init(cls, *args, **kwargs):
        """
        This method implements the creation of a `ListSerializer` parent
        class when `many=True` is used. You can customize it if you need to
        control which keyword arguments are passed to the parent, and
        which are passed to the child.

        Note that we're over-cautious in passing most arguments to both parent
        and child classes in order to try to cover the general case. If you're
        overriding this method you'll probably want something much simpler, eg:

        @classmethod
        def many_init(cls, *args, **kwargs):
            kwargs['child'] = cls()
            return CustomListSerializer(*args, **kwargs)
        """
        allow_empty = kwargs.pop('allow_empty', None)
        child_serializer = cls(*args, **kwargs)
        list_kwargs = {
            'child': child_serializer,
        }
        if allow_empty is not None:
            list_kwargs['allow_empty'] = allow_empty
        list_kwargs.update({
            key: value for key, value in kwargs.items()
            if key in LIST_SERIALIZER_KWARGS
        })
        meta = getattr(cls, 'Meta', None)
        list_serializer_class = getattr(meta, 'list_serializer_class', GRPCListSerializer)
        return list_serializer_class(*args, **list_kwargs)

    def _convert_to_message(self, validated_data, request):
        return ParseDict(validated_data, request, ignore_unknown_fields=True)

    def _process_request(self, validated_data):
        if not self.Meta.host:
            raise AssertionError('Meta has no host specified')
        if not self.Meta.stub:
            raise AssertionError('Meta has no stub specified')
        if not self.Meta.method:
            raise AssertionError('Meta has no method specified')
        if not self.Meta.request:
            raise AssertionError('Meta has no request specified')

        if self.INCLUDE_TOKEN_DATA:
            account_id = validated_data.get('account_id')
            pls_token = getattr(self.context['request'], 'pls_token', None)
            if pls_token:
                if 'permissions' in pls_token:
                    pls_token.pop('permissions')
                validated_data.update(pls_token)
                if account_id:
                    validated_data.update({'account_id': account_id})
        is_landing = getattr(self.context['request'], 'is_landing', False)
        if is_landing:
            validated_data['account_id'] = 24
            validated_data['user_id'] = 2

        request = self.context['request']
        metadata = metadata_from_request(request)
        with grpc.insecure_channel(self.Meta.host) as channel:
            stub = self.Meta.stub(channel)
            req = self._convert_to_message(validated_data, self.Meta.request())
            _call = getattr(stub, self.Meta.method)
            response = _call(req, metadata=metadata)
            data = MessageToDict(response, preserving_proto_field_name=True, including_default_value_fields=True)
            if hasattr(self, '_enhance_data'):
                data = self._enhance_data(data)
            return data

    @grpc_errors_wrapper
    def create(self, *args, **kwargs):
        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.save()`.'
        )

        assert not self.errors, (
            'You cannot call `.save()` on a serializer with invalid data.'
        )

        assert not hasattr(self, '_data'), (
            "You cannot call `.save()` after accessing `serializer.data`."
            "If you need to access data before committing to the database then "
            "inspect 'serializer.validated_data' instead. "
        )

        validated_data = dict(
            list(self.validated_data.items()) +
            list(kwargs.items())
        )
        self.instance = self._process_request(validated_data)
        return self.instance

    @grpc_errors_wrapper
    def delete(self, *args, **kwargs):
        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.save()`.'
        )

        assert not self.errors, (
            'You cannot call `.save()` on a serializer with invalid data.'
        )

        assert not hasattr(self, '_data'), (
            "You cannot call `.save()` after accessing `serializer.data`."
            "If you need to access data before committing to the database then "
            "inspect 'serializer.validated_data' instead. "
        )

        validated_data = dict(
            list(self.validated_data.items()) +
            list(kwargs.items())
        )
        self.instance = self._process_request(validated_data)
        return self.instance

    @grpc_errors_wrapper
    def list(self, *args, **kwargs):
        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.save()`.'
        )

        assert not self.errors, (
            'You cannot call `.save()` on a serializer with invalid data.'
        )

        assert not hasattr(self, '_data'), (
            "You cannot call `.save()` after accessing `serializer.data`."
            "If you need to access data before committing to the database then "
            "inspect 'serializer.validated_data' instead. "
        )

        validated_data = dict(
            list(self.validated_data.items()) +
            list(kwargs.items())
        )
        self.instance = self._process_request(validated_data)
        return self.instance

    @grpc_errors_wrapper
    def retrieve(self, *args, **kwargs):
        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.save()`.'
        )

        assert not self.errors, (
            'You cannot call `.save()` on a serializer with invalid data.'
        )

        assert not hasattr(self, '_data'), (
            "You cannot call `.save()` after accessing `serializer.data`."
            "If you need to access data before committing to the database then "
            "inspect 'serializer.validated_data' instead. "
        )

        validated_data = dict(
            list(self.validated_data.items()) +
            list(kwargs.items())
        )
        self.instance = self._process_request(validated_data)
        return self.instance

    @grpc_errors_wrapper
    def update(self, *args, **kwargs):
        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.save()`.'
        )

        assert not self.errors, (
            'You cannot call `.save()` on a serializer with invalid data.'
        )

        assert not hasattr(self, '_data'), (
            "You cannot call `.save()` after accessing `serializer.data`."
            "If you need to access data before committing to the database then "
            "inspect 'serializer.validated_data' instead. "
        )

        validated_data = dict(
            list(self.validated_data.items()) +
            list(kwargs.items())
        )
        self.instance = self._process_request(validated_data)
        return self.instance


    def to_representation(self, instance):
        return instance


class GRPCListSerializer(serializers.ListSerializer):

    def is_valid(self, raise_exception=False):
        if not hasattr(self, '_validated_data'):
            try:
                self.child.is_valid()
                self._validated_data = self.child._validated_data
            except ValidationError as exc:
                self._errors = exc.detail
            else:
                self._errors = []

        if self._errors and raise_exception:
            raise ValidationError(self.errors)

        return not bool(self._errors)

    def _process_request(self, validated_data):
        if not self.child.Meta.host:
            raise AssertionError('Meta has no host specified')
        if not self.child.Meta.stub:
            raise AssertionError('Meta has no stub specified')
        if not self.child.Meta.method:
            raise AssertionError('Meta has no method specified')
        if not self.child.Meta.request:
            raise AssertionError('Meta has no request specified')

        if self.child.INCLUDE_TOKEN_DATA:
            account_id = validated_data.get('account_id')
            pls_token = getattr(self.context['request'], 'pls_token', None)
            if pls_token:
                if 'permissions' in pls_token:
                    pls_token.pop('permissions')
                validated_data.update(pls_token)
                if account_id:
                    validated_data.update({'account_id': account_id})


        request = self.context['request']
        metadata = metadata_from_request(request)

        with grpc.insecure_channel(self.child.Meta.host) as channel:
            stub = self.child.Meta.stub(channel)
            req = self.child._convert_to_message(validated_data, self.child.Meta.request())
            _call = getattr(stub, self.child.Meta.method)
            data = MessageToDict(_call(req, metadata=metadata), preserving_proto_field_name=True, including_default_value_fields=True)
            if hasattr(self.child, '_enhance_data'):
                data = self.child._enhance_data(data)
            return data

    @grpc_errors_wrapper
    def list(self, *args, **kwargs):
        assert hasattr(self, '_errors'), (
            'You must call `.is_valid()` before calling `.save()`.'
        )

        assert not self.errors, (
            'You cannot call `.save()` on a serializer with invalid data.'
        )

        assert not hasattr(self, '_data'), (
            "You cannot call `.save()` after accessing `serializer.data`."
            "If you need to access data before committing to the database then "
            "inspect 'serializer.validated_data' instead. "
        )

        validated_data = dict(
            list(self.child.validated_data.items()) +
            list(kwargs.items()) +
            list(self.validated_data.items())
        )
        self.instance = self._process_request(validated_data)
        return self.instance

    def to_representation(self, data):
        return data

    @property
    def data(self):
        return ReturnDict(self.instance, serializer=self)
