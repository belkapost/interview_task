from typing import TypedDict, Optional, List


class PLSUser(TypedDict):
    token_type: str
    exp: int
    jti: str
    user_id: int
    account_id: int
    account_owner_id: int
    sub: int
    modules: List[str]
    is_admin: Optional[bool]
