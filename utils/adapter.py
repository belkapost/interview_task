from inspect import isclass

from google.protobuf.json_format import MessageToDict, ParseDict


def parse_message(message):
    return MessageToDict(message, including_default_value_fields=True, preserving_proto_field_name=True)


def dict_to_message(data, message):
    if isclass(message):
        message = message()
    if type(data) is list:
        return [ParseDict(_, message, ignore_unknown_fields=True) for _ in data]
    return ParseDict(data, message, ignore_unknown_fields=True)
