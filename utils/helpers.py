import concurrent.futures
from typing import Mapping, Callable


class AsyncWrapperTask(Mapping):
    name: str
    func: Callable
    kwargs: dict


def async_wrapper(tasks: [AsyncWrapperTask]) -> dict:
    results = {}

    def _wrapper(name, func, kwargs):
        return {name: func(**kwargs)}

    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        futures = {executor.submit(_wrapper, name=task['name'], func=task['func'], kwargs=task['kwargs']) for task in tasks}
        for future in concurrent.futures.as_completed(futures):
            data = future.result()
            results.update(data)
    return results
