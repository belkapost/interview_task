from django.urls import path, include
from apps.calculation.urls import urlpatterns as calculation_patterns

urlpatterns = [
    path('calculation/', include(calculation_patterns)),
]
