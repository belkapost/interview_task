from django.urls import path

from apps.calculation.views import StartCalculationView, CalculationResultView, StartMultiCalculationView, \
    CreateTaskToTerminalView, CreateForeignTaskView, CreateTaskTerminalTerminalView, CreateTaskFromTerminalView, \
    CreateTaskFromCastView, CreateSingleTaskWithReturnView, GetMultiGroupView, GetRateResultView, GetBetterRateView, \
    RecalcForTerminalsView

urlpatterns = [
    path('start/', StartCalculationView.as_view()),
    path('multi/start/', StartMultiCalculationView.as_view()),
    path('terminal/start/', CreateTaskToTerminalView.as_view()),
    path('terminal_terminal/start/', CreateTaskTerminalTerminalView.as_view()),
    path('from_terminal/start/', CreateTaskFromTerminalView.as_view()),
    path('foreign/start/', CreateForeignTaskView.as_view()),
    path('<str:task_id>/results/', CalculationResultView.as_view()),
    path('<str:rate_result_id>/result/detail/', GetRateResultView.as_view()),
    path('<str:rate_result_id>/result/better/', GetBetterRateView.as_view()),
    path('<str:rate_result_id>/terminals/recalc/', RecalcForTerminalsView.as_view()),
    path('from_cast/start/', CreateTaskFromCastView.as_view()),
    path('start/with_return/', CreateSingleTaskWithReturnView.as_view()),
    path('multi/<str:multi_group_id>/result/detail/', GetMultiGroupView.as_view()),
]
