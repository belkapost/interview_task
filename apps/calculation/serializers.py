from django.conf import settings
from rest_framework import serializers

from apps.calculation.cast_task import create_simple_task
from pls_grpc_utils.serializer import GrpcSerializer
from rates_pb2 import GetSingleTaskResultRequest, CreateSingleTaskNewRequest, \
    CreateMultiSimpleTaskRequest, CreateTaskToTerminalRequest, CreateTaskTerminalTerminalRequest, \
    CreateTaskFromTerminalRequest, CreateSingleTaskWithReturnRequest, GetMultiGroupRequest, GetRateResultRequest, \
    GetBetterRateRequest, RecalcForTerminalsRequest
from rates_pb2_grpc import RatesStub


class CalculationMeta:
    host = settings.PLS_RATES_HOST
    stub = RatesStub
    request = None
    method = None


class StartCalculationSerializer(GrpcSerializer):

    sender_address_id = serializers.CharField()
    receiver_address_id = serializers.CharField()
    cargo_id = serializers.CharField()
    return_cargo_id = serializers.CharField(allow_blank=True, allow_null=True, required=False)

    calc_terminal = serializers.BooleanField(default=False)

    class Meta(CalculationMeta):
        request = CreateSingleTaskNewRequest
        method = 'CreateSingleTaskNew'


class CalculationResultSerializer(GrpcSerializer):
    single_task_id = serializers.CharField()

    class Meta(CalculationMeta):
        request = GetSingleTaskResultRequest
        method = 'GetSingleTaskResult'


class MultiTaskSerializer(GrpcSerializer):
    destination_id = serializers.IntegerField()
    sender_address_id = serializers.CharField()
    receiver_address_id = serializers.CharField()
    cargo_id = serializers.CharField()


class StartMultiCalculationSerializer(GrpcSerializer):
    tasks = MultiTaskSerializer(many=True)

    class Meta(CalculationMeta):
        request = CreateMultiSimpleTaskRequest
        method = 'CreateMultiSimpleTask'


class CreateTaskToTerminalSerializer(GrpcSerializer):
    sender_address_id = serializers.CharField()
    terminal_id = serializers.IntegerField()
    cargo_id = serializers.CharField()


    class Meta(CalculationMeta):
        request = CreateTaskToTerminalRequest
        method = 'CreateTaskToTerminal'


class CreateForeignTaskSerializer(StartCalculationSerializer):

    class Meta(CalculationMeta):
        request = CreateSingleTaskNewRequest
        method = 'CreateForeignTask'


class CreateTaskTerminalTerminalSerializer(GrpcSerializer):
    sender_terminal_id = serializers.CharField()
    receiver_terminal_id = serializers.IntegerField()
    cargo_id = serializers.CharField()

    class Meta(CalculationMeta):
        request = CreateTaskTerminalTerminalRequest
        method = 'CreateTaskTerminalTerminal'


class CreateTaskFromTerminalSerializer(GrpcSerializer):
    sender_terminal_id = serializers.IntegerField()
    receiver_address_id = serializers.CharField()
    cargo_id = serializers.CharField()

    class Meta(CalculationMeta):
        request = CreateTaskFromTerminalRequest
        method = 'CreateTaskFromTerminal'


class CreateTaskFromCastSerializer(GrpcSerializer):
    sender_contact_cast_id = serializers.CharField()
    receiver_contact_cast_id = serializers.CharField()
    cargo_cast_id = serializers.CharField()

    def _process_request(self, validated_data):
        request = self.context['request']
        pls_token = request.pls_token
        return create_simple_task(**validated_data, pls_user=pls_token)


class CreateSingleTaskWithReturnSerializer(StartCalculationSerializer):
    return_cargo_id = serializers.CharField()

    class Meta(StartCalculationSerializer.Meta):
        request = CreateSingleTaskWithReturnRequest
        method = 'CreateSingleTaskWithReturn'


class GetMultiGroupSerializer(GrpcSerializer):
    multi_group_id = serializers.CharField()

    class Meta(CalculationMeta):
        request = GetMultiGroupRequest
        method = 'GetMultiGroup'


class GetRateResultSerializer(GrpcSerializer):
    rate_result_id = serializers.CharField()

    class Meta(CalculationMeta):
        request = GetRateResultRequest
        method = 'GetRateResult'


class GetBetterRateSerializer(GrpcSerializer):
    current_rate_result_id = serializers.CharField()

    class Meta(CalculationMeta):
        request = GetBetterRateRequest
        method = 'GetBetterRate'


class RecalcForTerminalsSerializer(GrpcSerializer):
    rate_result_id = serializers.CharField()
    sender_terminal_id = serializers.IntegerField(required=False, allow_null=True)
    receiver_terminal_id = serializers.IntegerField(required=False, allow_null=True)

    class Meta(CalculationMeta):
        request = RecalcForTerminalsRequest
        method = 'RecalcForTerminals'
