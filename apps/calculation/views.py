from rest_framework.decorators import permission_classes

from apps.calculation.serializers import StartCalculationSerializer, CalculationResultSerializer, \
    StartMultiCalculationSerializer, CreateTaskToTerminalSerializer, CreateForeignTaskSerializer, \
    CreateTaskTerminalTerminalSerializer, CreateTaskFromTerminalSerializer, CreateTaskFromCastSerializer, \
    CreateSingleTaskWithReturnSerializer, GetMultiGroupSerializer, GetRateResultSerializer, GetBetterRateSerializer, \
    RecalcForTerminalsSerializer
from apps.shipping.serializers import ComplexCreationFromTerminalSerializer
from pls_grpc_utils.permissions import IsPLSAuthenticated, IsHasModulePermission, LandingOrPLSAuthenticated
from pls_grpc_utils.views import GRPCCreateView, GRPCRetrieveView, GRPCUpdateView


class StartCalculationView(GRPCCreateView):
    serializer_class = StartCalculationSerializer
    permission_classes = LandingOrPLSAuthenticated,


class CalculationResultView(GRPCRetrieveView):
    serializer_class = CalculationResultSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission
    lookup_url_kwarg = 'task_id'
    lookup_field = 'single_task_id'


class StartMultiCalculationView(GRPCCreateView):
    serializer_class = StartMultiCalculationSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class CreateTaskToTerminalView(GRPCCreateView):
    serializer_class = CreateTaskToTerminalSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class CreateForeignTaskView(GRPCCreateView):
    serializer_class = CreateForeignTaskSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class CreateTaskTerminalTerminalView(GRPCCreateView):
    serializer_class = CreateTaskTerminalTerminalSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class CreateTaskFromTerminalView(GRPCCreateView):
    serializer_class = CreateTaskFromTerminalSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class ComplexCreationFromTerminalView(GRPCCreateView):
    serializer_class = ComplexCreationFromTerminalSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class CreateTaskFromCastView(GRPCCreateView):
    serializer_class = CreateTaskFromCastSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class CreateSingleTaskWithReturnView(GRPCCreateView):
    serializer_class = CreateSingleTaskWithReturnSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission


class GetMultiGroupView(GRPCRetrieveView):
    serializer_class = GetMultiGroupSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission
    lookup_url_kwarg = 'multi_group_id'
    lookup_field = 'multi_group_id'


class GetRateResultView(GRPCRetrieveView):
    serializer_class = GetRateResultSerializer
    permission_classes = IsHasModulePermission, IsPLSAuthenticated
    lookup_url_kwarg = 'rate_result_id'
    lookup_field = 'rate_result_id'


class GetBetterRateView(GRPCRetrieveView):
    serializer_class = GetBetterRateSerializer
    permission_classes = IsHasModulePermission, IsPLSAuthenticated
    lookup_url_kwarg = 'rate_result_id'
    lookup_field = 'current_rate_result_id'


class RecalcForTerminalsView(GRPCUpdateView):
    serializer_class = RecalcForTerminalsSerializer
    permission_classes = IsPLSAuthenticated, IsHasModulePermission
    lookup_url_kwarg = 'rate_result_id'
    lookup_field = 'rate_result_id'
