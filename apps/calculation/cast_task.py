import grpc
from django.conf import settings
from rest_framework.exceptions import ValidationError

from cargo_casting_pb2 import GetWaybillCargoCastRequest
from cargo_casting_pb2_grpc import CargoCastingStub
from cargoes_pb2 import CreateCargoRequest, CreateInterCargoRequest
from cargoes_pb2_grpc import CargoesStub
from contacts_casting_pb2 import GetWaybillContactCastRequest
from contacts_casting_pb2_grpc import ContactCastingStub
from contacts_pb2 import CreateAddressCastRequest, CreateContactCastRequest, \
    CreateTransliterateAddressContactCastRequest, CreateForeignAddressContactCastRequest
from contacts_pb2_grpc import ContactsStub
from rates_pb2 import CreateSingleTaskNewRequest
from rates_pb2_grpc import RatesStub
from utils.adapter import parse_message, dict_to_message


def get_contact_data(contact_cast_id: str) -> dict:
    with grpc.insecure_channel(settings.ADMIN_CONTACTS_HOST) as channel:
        stub = ContactCastingStub(channel)
        response = stub.GetWaybillContactCast(
            GetWaybillContactCastRequest(contact_cast_id=contact_cast_id)
        )
        return parse_message(response)


def get_cargo_data(cargo_cast_id: str) -> dict:
    with grpc.insecure_channel(settings.ADMIN_CARGOES_HOST) as channel:
        stub = CargoCastingStub(channel)
        response = stub.GetWaybillCargoCast(GetWaybillCargoCastRequest(
            cargo_cast_id=cargo_cast_id
        ))
        return parse_message(response)


def create_full_domestic_cast(contact_data: dict, account_id: int, side: str) -> dict:
    with grpc.insecure_channel(settings.PLS_CONTACTS_HOST) as channel:
        stub = ContactsStub(channel)
        address = stub.CreateAddressCast(dict_to_message(
            {
                **contact_data,
                'account_id': account_id,
            },
            CreateAddressCastRequest
            )
        )

        contact = stub.CreateContactCast(dict_to_message(
            {
                **contact_data,
                'account_id': account_id,
                'address_cast_id': address.address_cast_id,
                'side': side,
            },
            CreateContactCastRequest
        ))
        return {
            **contact_data,
            'account_id': account_id,
            'address_cast_id': address.address_cast_id,
            'contact_info_cast_id': contact.contact_info_cast_id,
            'side': side,
        }


def create_cargo(cargo: dict, account_id: int) -> dict:
    with grpc.insecure_channel(settings.PLS_CARGOES_HOST) as channel:
        stub = CargoesStub(channel)
        print(cargo)
        response = stub.CreateCargo(dict_to_message({
            'packages': cargo['packages'],
            'insurance_request': 1 if cargo.get('insurance') else 0,
            'insurance_value_rate': cargo.get('insurance', {}).get('insurance_value_rate'),
            'account_id': account_id,
        }, CreateCargoRequest))
        return parse_message(response)


def create_transliterated_cast(contact_data: dict, account_id: int, side: str) -> dict:
    with grpc.insecure_channel(settings.PLS_CONTACTS_HOST) as channel:
        stub = ContactsStub(channel)
        data = stub.CreateTransliterateAddressContactCast(dict_to_message(
            {
                **contact_data,
                'account_id': account_id,
            },
            CreateTransliterateAddressContactCastRequest
            )
        )

        return {
            **contact_data,
            'account_id': account_id,
            'address_cast_id': data.address_cast_id,
            'contact_info_cast_id': data.contact_info_cast_id,
            'side': side,
        }


def create_foreign_cast(contact_data: dict, account_id: int, side: str) -> dict:
    with grpc.insecure_channel(settings.PLS_CONTACTS_HOST) as channel:
        stub = ContactsStub(channel)
        data = stub.CreateForeignAddressContactCast(dict_to_message(
            {
                **contact_data,
                'account_id': account_id,
            },
            CreateForeignAddressContactCastRequest
            )
        )

        return {
            **contact_data,
            'account_id': account_id,
            'address_cast_id': data.address_cast_id,
            'contact_info_cast_id': data.contact_info_cast_id,
            'side': side,
        }


def create_foreign_cargo(cargo: dict, account_id: int) -> dict:
    with grpc.insecure_channel(settings.PLS_CARGOES_HOST) as channel:
        stub = CargoesStub(channel)
        print(cargo)
        response = stub.CreateInterCargo(dict_to_message({
            'packages': cargo['packages'],
            'insurance_request': 1 if cargo.get('insurance') else 0,
            'insurance_value_rate': cargo.get('insurance', {}).get('insurance_value_rate'),
            'account_id': account_id,
        }, CreateInterCargoRequest))
        return parse_message(response)


def create_domestic_task(sender_data: dict, receiver_data: dict, cargo_data: dict, pls_user: dict) -> dict:
    sender = create_full_domestic_cast(sender_data, pls_user['account_id'], 'sender')
    print('sender')
    receiver = create_full_domestic_cast(receiver_data, pls_user['account_id'], 'receiver')
    print('receiver')
    cargo = create_foreign_cargo(cargo_data, pls_user['account_id'])
    print('cargo')
    data = {
        'sender': sender,
        'receiver': receiver,
        'cargo': cargo,
    }

    with grpc.insecure_channel(settings.PLS_RATES_HOST) as channel:
        stub = RatesStub(channel)
        response = stub.CreateSingleTaskNew(
            CreateSingleTaskNewRequest(
                sender_address_id=sender['address_cast_id'],
                receiver_address_id=receiver['address_cast_id'],
                cargo_id=cargo['cargo_id'],
                account_id=pls_user['account_id'],
                user_id=pls_user['user_id']
            )
        )
        data['single_task_id'] = response.single_task_id
    return data


def create_international_task(sender_data: dict, receiver_data: dict, cargo_data: dict, pls_user: dict) -> dict:
    if sender_data['iso'] == 'RU':
        sender = create_transliterated_cast(sender_data, pls_user['account_id'], 'sender')
    else:
        sender = create_foreign_cast(sender_data, pls_user['account_id'], 'foreign_sender')
    if receiver_data['iso'] == 'RU':
        receiver = create_transliterated_cast(receiver_data, pls_user['account_id'], 'receiver')
    else:
        receiver = create_foreign_cast(receiver_data, pls_user['account_id'], 'foreign_receiver')
    cargo = create_cargo(cargo_data, pls_user['account_id'])

    data = {
        'sender': sender,
        'receiver': receiver,
        'cargo': cargo,
    }

    with grpc.insecure_channel(settings.PLS_RATES_HOST) as channel:
        stub = RatesStub(channel)
        response = stub.CreateForeignTask(
            CreateSingleTaskNewRequest(
                sender_address_id=sender['address_cast_id'],
                receiver_address_id=receiver['address_cast_id'],
                cargo_id=cargo['cargo_id'],
                account_id=pls_user['account_id'],
                user_id=pls_user['user_id']
            )
        )
        data['single_task_id'] = response.single_task_id
    return data


def create_simple_task(sender_contact_cast_id: str, receiver_contact_cast_id: str, cargo_cast_id: str,
                       pls_user: dict) -> dict:
    sender_data = get_contact_data(sender_contact_cast_id)
    receiver_data = get_contact_data(receiver_contact_cast_id)
    cargo_data = get_cargo_data(cargo_cast_id)
    if sender_data['iso'] == 'RU' and receiver_data['iso'] == 'RU':
        return create_domestic_task(sender_data, receiver_data, cargo_data, pls_user)
    raise ValidationError({'error': 'unsupported'})
